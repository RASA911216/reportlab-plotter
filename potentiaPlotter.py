#######################################################################
#                        Potentia Documentgen
#######################################################################
                #This class generates documents in PDF or other formats for the Potentia API.

#######################################################################
#Import classes to generate a new pdf document.
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas

#Import Plot generation libraries.
from reportlab.lib.colors import PCMYKColor
from reportlab.graphics.shapes import Drawing
from reportlab.graphics.charts.barcharts import VerticalBarChart

class Documentgen:
    width = 0
    height = 0
    canv = None
    def __init__(self, storagePath, pageSize=letter):
        try:
            self.canv = canvas.Canvas(storagePath, pagesize=pageSize)
            width, height = letter
        except Exception as exc:
            print("Unable to create a new document. Error: ")
            print(exc)

    #Function to generate a new bar chart.
    #Arguments:
        #data --- Int Array --- What will be plotted
        #categoryNames --- String Array --- Categories of each group to plot
    def plotFromGraph(self, drawingWidth=600, drawingHeight=550, pltData = None, categoryNames = None):
        try:
            d = Drawing(drawingWidth, drawingHeight)
            bar = VerticalBarChart()
            bar.x = 50
            bar.y = 600
            bar.data = pltData

            bar.categoryAxis.categoryNames = categoryNames

            #bar.bars[0].fillColor   = PCMYKColor(0,100,100,40,alpha=85)
            #bar.bars[1].fillColor   = PCMYKColor(23,51,0,4,alpha=85)
            bar.bars.fillColor       = PCMYKColor(100,0,90,50,alpha=85)

            #d.add(bar, '')
            d.add(bar, '')
            d.drawOn(self.canv, 0, 0)
        except Exception as exc:
            print("Error creating plotFromGraph")
            print(exc)

    #Function to create a new PDF file.
    def createPDF(self):
        try:
            self.canv.save()
        except Exception as exc:
            print("Error generating pdf")
            print(exc)

if __name__=='__main__':
    #Generate a new Canvas object.
    doc = Documentgen('./test.pdf', letter)


    #Create a new graph.
    data = [
        [1, 10, 0],
    ]
    categoryNames = ['Test1', "Data2", "Sakura"]

    doc.plotFromGraph(600, 550, data, categoryNames)
    #Generate pdf.
    doc.createPDF()
