from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
#Import Plot generation libraries.
from reportlab.lib.colors import PCMYKColor
from reportlab.graphics.shapes import Drawing
from reportlab.graphics.charts.barcharts import VerticalBarChart

def create_bar_chart(can):
    d = Drawing(300, 250)
    bar = VerticalBarChart()
    bar.x = 50
    bar.y = 85
    data = [
        [0, 1, 2, 3, 5],
        [10, 5, 2, 4, 1],
        [0, 0, 0, 0, 0],
        [10, 10, 10, 10, 10]
    ]
    bar.data = data

    bar.categoryAxis.categoryNames = ["Data 1", "Flor", "Triangulo", "Neurona", "Rem"]

    bar.bars[0].fillColor   = PCMYKColor(0,100,100,40,alpha=85)
    bar.bars[1].fillColor   = PCMYKColor(23,51,0,4,alpha=85)
    bar.bars.fillColor       = PCMYKColor(100,0,90,50,alpha=85)

    d.add(bar, '')
    d.drawOn(can, 0, 0)

#Generate a new Canvas object.
c = canvas.Canvas("hello.pdf", pagesize=letter)
width, height = letter

#Create a new graph.
create_bar_chart(c)

c.drawString(100, 100, "Hello World!!!")
c.save()
